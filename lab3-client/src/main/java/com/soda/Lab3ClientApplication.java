package com.soda;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication
public class Lab3ClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(Lab3ClientApplication.class, args);
	}
}
