package com.soda;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WordsController {
    @Value("${words}")
    private String words;

    @RequestMapping("/")
    public String getWord() {
        String[] wordArray = words.split(",");
        return wordArray[(int) Math.ceil(Math.random() * wordArray.length -1)];
    }
}
